<?php
namespace App\Controller;

use App\Entity\Category;
use App\Entity\Products;
use App\Form\CategoryChoiceType;
use App\Form\RegistrationType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use function Symfony\Component\DependencyInjection\Loader\Configurator\param;

class MainController extends AbstractController
{
    /**
     * @param Request $request
     * @return Response
     * @Route ("/", name="index")
     */
    public function index(Request $request): Response
    {
        $category = new Products();
        $products = $this->getDoctrine()->getRepository(Products::class)->findAllJoinToCategory();
        $form = $this->createForm(CategoryChoiceType::class, $category);
//        dd($request->get('category_choice'));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $idCategory = ($form->get('Category')->getViewData());
            $products = $this->getDoctrine()->getRepository(Products::class)->findOneJoinToCategory($idCategory);
        }
        if (!$products)
        {
            return $this->render(
                'emptyView.html.twig',
            [
                'form' => $form->createView(),
            ]);
        }
        return $this->render(
            'view.html.twig',
            [
                'form' => $form->createView(),
                'products' => $products
            ]
        );
    }

    /**
     * @param Request $request
     * @return Response
     * @Route ("/create", name="create")
     */
    public function create(Request $request ): Response
    {
        $product = new Products();
        $category = $this->getDoctrine()->getRepository(Category::class)->findAll();
//        dd($category);
        $form = $this->createForm(RegistrationType::class, $product);
//        dd($request);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $this->controlPicture($form, $product);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($product);
            $entityManager->flush();
            return $this->redirectToRoute('index');
        }
        return $this->render('registration/index.html.twig', [
            'form' => $form->createView(),

        ]);
    }

    /**
     * @param string $id
     * @param Request $request
     * @return Response
     * @Route ("/update/{id}", name="update")
     */
    public function update(string $id, Request $request): Response
    {
        $product = $this->getDoctrine()->getRepository(Products::class);
        $product = $product->find($id);
        $oldPicture = $product->getPicture();
        $form = $this->createForm(RegistrationType::class, $product);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $this->controlPicture($form, $product);
            $this->removePicture($oldPicture);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            return $this->redirectToRoute('index');
        }
        return $this->render(
            'registration/index.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @param int $id
     * @return Response
     * @Route("/delete/{id}", name="delete")
     */
    public function delete(int $id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $product = $this->getDoctrine()->getRepository(Products::class);
        $product = $product->find($id);
        $em->remove($product);
        $em->flush();
        return $this->redirectToRoute('index');
    }

    private function controlPicture($form, $product)
    {
        $registerPictureProduct = $form->get('picture')->getData();
        if ($registerPictureProduct){
            $newPictureFileName = md5(uniqid(rand())) . "." . $registerPictureProduct->guessExtension();
            $pictureProductDestination = $this->getParameter('product_pictures_dir');

            try {
                $registerPictureProduct->move($pictureProductDestination , $newPictureFileName);
            } catch (FileException $e) {
                throw new HttpException(500, 'An error was catch during the upload');
            }
        } else { $newPictureFileName = 'default.png';
        }
        $product->setPicture($newPictureFileName);
    }

    private function removePicture($oldPicture)
    {
        if ($oldPicture != 'default.png') {
            $fs = new Filesystem();
            try {
                $fs->remove($this->getParameter('product_pictures_dir') . '/' . $oldPicture);
            } catch (IOException $e) {
                throw new HttpException(500, 'AN error occurred during file remove');
            }
        }
    }
}