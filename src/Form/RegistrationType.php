<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Products;
use Nextend\Framework\Form\Element\Select;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\ChoiceList;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('name')
            ->add('description')
            ->add('price')
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'nameCategory',
            ])
            ->add('picture', FileType::class , [
                'data_class' => null,
                'label' => 'Product Picture',
                'required' => false,
                'constraints' =>
                [
                    new File([
                        'maxSize' => '2M',
                        'mimeTypes' =>
                        [
                            'image/png',
                            'image/jpg',
                        ], 'mimeTypesMessage' => 'Please upload a valid picture!',
                    ])
                ],
            ])
            ->add('Save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Products::class,
        ]);
    }
}
