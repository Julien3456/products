<?php

namespace App\Repository;

use App\Entity\Products;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Response;

/**
 * @method Products|null find($id, $lockMode = null, $lockVersion = null)
 * @method Products|null findOneBy(array $criteria, array $orderBy = null)
 * @method Products[]    findAll()
 * @method Products[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Products::class);
    }

    public function findAllJoinToCategory()
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery(
            'SELECT p.id, p.name, p.description, p.price, p.picture, c.nameCategory
            FROM App\Entity\Products p
            INNER JOIN App\Entity\Category c
            WHERE p.category = c.id'
        );
//        dd($query->getResult());
        return $query->getResult();

    }
    public function findOneJoinToCategory($id)
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery(
            'SELECT p.id, p.name, p.description, p.price, p.picture, c.nameCategory
            FROM App\Entity\Products p
            INNER JOIN App\Entity\Category c
            WHERE p.category = c.id
            AND p.category IN (:id)'
        )
        ->setParameter('id', $id);
//        dd($query->getResult());
        return $query->getResult();

    }

    // /**
    //  * @return Products[] Returns an array of Products objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Products
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
